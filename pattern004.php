
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
    
    <div class="container">
        <div class="row">
            <div class="col-sm-8" style="font-weight: bold;font-size: 30px">
                <h2>Output:</h2><br>
         
              <?php
if(isset($_POST['btn'])){
  ?>
                <pre><?php
    $n = $_POST['number'];
    for($i=1;$i<=$n;$i++){
        for($j=$i;$j<=$n;$j++){
           
            echo "*";
        }
        echo "<br>";
        for($j=1;$j<=$i;$j++){
            
            echo '&nbsp;';
        }
    }
    
}

?>
                </pre>
            </div>
           
            <div class="col-sm-4 ">
                 <h2>Enter the number:</h2>
                 <form action="" method="post">
    <div class="form-group">
      <br>
      <input type="text" class="form-control" id="number" placeholder="Enter the number" name="number">
    </div>
                     <button type="submit" name="btn" class="btn btn-success">Submit</button>
  </form>             
            </div>
           
        </div> 
    </div>
    
</body>
</html>
